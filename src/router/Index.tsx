import { Route, Routes } from "react-router-dom";
import Home from "../view/page/Home";
import Movie from "../view/page/Movie";
import DetailMovie from "../view/page/detail/DetailMovie";

const Router = () => {
  return (
    <Routes>
      <Route path="" element={<Home />} />
      <Route path="/movie" element={<Movie />} />
      <Route path="/detailmovie/:id" element={<DetailMovie />} />
    </Routes>
  );
};
export default Router;
