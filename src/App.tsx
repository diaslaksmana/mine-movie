import "./App.css";
import BaseLayout from "./view/layout/BaseLayout";

function App() {
  return (
    <>
      <BaseLayout />
    </>
  );
}

export default App;
