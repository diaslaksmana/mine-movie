import { useParams } from "react-router-dom";
import { GetMoviesResponse, MovieModel } from "../utils/interface/movie";
import { useEffect, useState } from "react";
import { fetchData } from "../utils/service/movie.service";
import { CastModel } from "../utils/interface/cast";
import { ReviewModel } from "../utils/interface/review";

const useFetchMovieDetail = () => {
  const params = useParams();
  const [dataMovie, setDataMovie] = useState<MovieModel>();
  const [similiar, setSimiliar] = useState<MovieModel>();
  const [genresbyId, setGenresbyId] = useState<GetMoviesResponse[]>([]);
  const [company, setCompany] = useState<MovieModel[]>([]);
  const [review, setReview] = useState<ReviewModel[]>([]);
  const [cast, setCast] = useState<CastModel[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();
  const [num, setNum] = useState(8);
  const fetcMovie = () => {
    async function getData() {
      try {
        const data = await fetchData(`movie/${params.id}`);
        setDataMovie(data);
        console.log(data);
        setGenresbyId(data.genres);
        setCompany(data.production_companies);
        console.log(data.genres);
        const cast = await fetchData(`movie/${params.id}/credits`);
        setCast(cast.cast);
        console.log(cast);
        const review = await fetchData(`movie/${params.id}/reviews`);
        setReview(review.results);
        console.log(review);
        const similiar = await fetchData(`movie/${params.id}/similar`);
        setSimiliar(similiar.results);
        console.log(similiar);
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    }
    getData();
  };
  // ratting
  const getProgressColor = (voteAverage: number = 0) => {
    if (voteAverage >= 7.5) {
      return "#21d07a";
    } else if (voteAverage >= 5.0) {
      return "#f2c94c";
    } else {
      return "#eb5757";
    }
  };

  const getRatingPercentage = (voteAverage: number = 0) => {
    return (voteAverage / 10) * 100;
  };
  // add more image cast
  const imagePerRow = 5;
  const handleMoreImage = () => {
    setNum(num + imagePerRow);
  };
  useEffect(() => {
    fetcMovie();
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return {
    dataMovie,
    isLoading,
    error,
    genresbyId,
    getProgressColor,
    getRatingPercentage,
    cast,
    handleMoreImage,
    num,
    company,
    review,
    similiar,
  };
};

export default useFetchMovieDetail;
