import { useEffect, useState } from "react";
import { GenreModel, MovieModel } from "../utils/interface/movie";
import { fetchData, fetchListGenre } from "../utils/service/movie.service";

const useFetchGeneralData = () => {
  const [dataBanner, setDataBanner] = useState<MovieModel[]>([]);
  const [dataMovie, setDataMovie] = useState<MovieModel[]>([]);
  const [genreMovie, setGenreMovie] = useState<GenreModel[]>([]);
  const [time, setTime] = useState<"day" | "week">("week");
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();

  const fetchSlider = async () => {
    async function getData() {
      try {
        const [data, genreList] = await Promise.all([
          fetchData("trending/all/week"),
          fetchListGenre("genre/movie/list"),
        ]);
        setGenreMovie(genreList);
        setDataBanner(data.results);
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    }
    getData();
  };

  const fetchMoviePopular = async () => {
    async function getData() {
      try {
        const data = await fetchData(`trending/movie/${time}`);
        setDataMovie(data.results);
      } catch (error) {
        setError(error);
      } finally {
        setTimeout(() => {
          setIsLoading(false);
        }, 60000);
      }
    }
    getData();
  };

  useEffect(() => {
    fetchSlider();
    fetchMoviePopular();
  }, [time]);

  return {
    dataBanner,
    isLoading,
    error,
    dataMovie,
    genreMovie,
    setTime,
    time,
  };
};

export default useFetchGeneralData;
