import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { BASE_URL, options } from "../utils/config/Config";
import { GenreModel, MovieModel } from "../utils/interface/movie";
import { fetchListGenre } from "../utils/service/movie.service";

const useFetchCategory = () => {
  const params = useParams();
  const [catMovie, setCatMovie] = useState<MovieModel[]>();
  const [listGenreMovie, setListGenreMovie] = useState<GenreModel[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();
  const [num, setNum] = useState(1);
  const [pages, setPages] = useState<any>();
  const [selectedGenreId, setSelectedGenreId] = useState<number | null>(null);
  const [sortBy, setSortBy] = useState<string>("popularity.desc");
  const apiKey = process.env.REACT_APP_API_KEY;
  const fetchCategoryMovie = () => {
    async function getData() {
      try {
        let url = `${BASE_URL}/discover/movie?api_key=${apiKey}&include_adult=false&include_video=false&language=en-US&page=${num}`;
        if (selectedGenreId) {
          url += `&with_genres=${selectedGenreId}`;
        } else if (params.id) {
          url += `&with_genres=${params.id}`;
        }
        // Add sorting based on user selection
        if (sortBy === "rating_lowest") {
          url += `&sort_by=vote_average.asc`;
        } else if (sortBy === "rating_highest") {
          url += `&sort_by=vote_average.desc`;
        }
        const response = await fetch(url, options);
        const data = await response.json();
        setCatMovie(data.results);
        console.log(data);
        setPages(data);
        const data2 = await fetchListGenre(`genre/movie/list`);
        setListGenreMovie(data2.genres);
        console.log(data2);
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    }
    getData();
  };
  // Function to handle sorting change
  const handleSortChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedSort = event.target.value;
    setSortBy(selectedSort);
  };
  const handleGenreChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const genreId = parseInt(event.target.value);
    setSelectedGenreId(genreId === 0 ? null : genreId);
    setNum(1);
  };
  const handleGenre = () => {
    setNum(1);
  };
  useEffect(() => {
    fetchCategoryMovie();
  }, [num, params.id, selectedGenreId, sortBy]);

  return {
    catMovie,
    isLoading,
    error,
    listGenreMovie,
    setNum,
    pages,
    handleGenre,
    handleGenreChange,
    handleSortChange,
  };
};

export default useFetchCategory;
