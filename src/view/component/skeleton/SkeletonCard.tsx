import React from "react";

interface SkeletonCardProps {
  width?: string;
  height?: string;
  borderRadius?: string;
}

const SkeletonCard: React.FC<SkeletonCardProps> = ({
  width = "auto",
  height = "100%",
  borderRadius = "5px",
}) => {
  return (
    <div className="skeleton-card" style={{ width, height, borderRadius }}>
      <div className="skeleton-content"></div>
    </div>
  );
};

export default SkeletonCard;
