// CircleProgressRating.tsx
import React from "react";

interface CircleProgressRatingProps {
  rating: number;
}

const CircleProgressRating: React.FC<CircleProgressRatingProps> = ({
  rating,
}) => {
  const radius = 20; // Radius of the circle
  const circumference = 2 * Math.PI * radius; // Circumference of the circle
  const progress = rating * 10; // Convert rating to percentage

  return (
    <svg height={radius * 2} width={radius * 2}>
      <circle
        stroke="#ddd" // Color of the circle
        strokeWidth="4" // Width of the circle
        fill="transparent"
        r={radius}
        cx={radius}
        cy={radius}
      />
      <circle
        stroke="#f90" // Color of the progress indicator
        strokeWidth="4" // Width of the progress indicator
        strokeLinecap="round" // Round line endings
        fill="transparent"
        r={radius}
        cx={radius}
        cy={radius}
        style={{
          strokeDasharray: `${circumference} ${circumference}`,
          strokeDashoffset: `${
            circumference - (progress / 100) * circumference
          }`,
        }}
      />
      <text
        x="50%"
        y="50%"
        textAnchor="middle"
        dy=".3em"
        fontSize="12px" // Font size of the rating
        fill="#333" // Text color
      >
        {rating}
      </text>
    </svg>
  );
};

export default CircleProgressRating;
