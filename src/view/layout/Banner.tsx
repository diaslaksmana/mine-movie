import { useState } from "react";
import useFetchGeneralData from "../../hook/useFetchGeneralData";
import {
  BACKDROP_SIZE,
  IMAGE_BASE_URL,
  POSTER_SIZE,
} from "../../utils/config/Config";
import { Link } from "react-router-dom";

const Banner = () => {
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const { dataBanner, isLoading, error } = useFetchGeneralData();

  if (error) {
    return <h1>Error</h1>;
  }

  return (
    <section id="banner" className="relative z-0">
      {isLoading ? (
        "Loading"
      ) : (
        <>
          {dataBanner.length > 0 && (
            <img
              src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${dataBanner[currentImageIndex].backdrop_path}`}
              // src={`https://www.gynprog.com.br/wp-content/uploads/2017/06/wood-blog-placeholder.jpg + ${dataBanner[currentImageIndex].backdrop_path}`}
              alt={
                dataBanner[currentImageIndex]?.media_type === "tv"
                  ? dataBanner[currentImageIndex]?.name
                  : dataBanner[currentImageIndex]?.title
              }
              className="h-screen w-full absolute object-cover object-center banner-image fade-in"
            />
          )}
        </>
      )}
      <div className="container mx-auto px-5 sm:px-0 z-30 relative">
        <div className="thumbnail-gallery grid sm:flex items-center justify-around sm:justify-between h-screen">
          {dataBanner.length > 0 && (
            <div className="overview ">
              <h1 className="text-3xl font-bold">
                {dataBanner[currentImageIndex]?.media_type === "tv"
                  ? dataBanner[currentImageIndex]?.name
                  : dataBanner[currentImageIndex]?.title}
              </h1>
              <h5 className="my-3">
                <b>Release date: </b>
                {dataBanner[currentImageIndex]?.media_type === "tv"
                  ? dataBanner[currentImageIndex]?.first_air_date
                  : dataBanner[currentImageIndex]?.release_date}
              </h5>
              <p className="text-sm max-w-[450px] my-3">
                {dataBanner[currentImageIndex].overview}
              </p>
              <br />
              <Link
                to={`/detailmovie/${dataBanner[currentImageIndex].id}`}
                className="bg-primary py-2 px-10 mt-5 rounded text-white"
              >
                Watch Movie
              </Link>
            </div>
          )}
          <div className="thumbnails-navigation flex items-center gap-2">
            {dataBanner.slice(0, 5).map((movie, index) => (
              <div
                className="box-img relative cursor-pointer"
                key={index}
                onClick={() => setCurrentImageIndex(index)}
              >
                <img
                  src={`${IMAGE_BASE_URL}${POSTER_SIZE}` + movie.poster_path}
                  alt={`Thumbnail ${index + 1}`}
                  className={`h-[100px] sm:h-[250px]  w-[150px] rounded object-cover object-center ${
                    index === currentImageIndex ? "active" : ""
                  }`}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};
export default Banner;
