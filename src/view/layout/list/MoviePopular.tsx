import { Link } from "react-router-dom";
import useFetchGeneralData from "../../../hook/useFetchGeneralData";
import { convertRated } from "../../../utils/helper/Helper";
import SkeletonCard from "../../component/skeleton/SkeletonCard";
import { IMAGE_BASE_URL, POSTER_SIZE } from "../../../utils/config/Config";

const MoviePopular = () => {
  const { dataMovie, isLoading, error, setTime, time } = useFetchGeneralData();
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div className="list my-5">
      <div className="container mx-auto px-5 sm:px-0">
        <div className="seaction-header mb-5 flex justify-between items-center">
          <h1 className="text-2xl font-semibold mb-2">Movie Popular</h1>
          <select
            value={time}
            onChange={(e) => setTime(e.target.value as "day" | "week")}
            className="border-[2px] min-w-[150px] border-primary text-primary bg-[transparent] py-2 px-4  hover:outline-0 focus:outline-0"
          >
            <option value="week">Weeks</option>
            <option value="day">Days</option>
          </select>
        </div>
        <div className="grid grid-cols-2 sm:grid-cols-7 gap-5">
          {isLoading &&
            [1, 2, 3, 4, 5, 6, 7].map((n) => <SkeletonCard key={n} />)}
          {dataMovie?.slice(0, 7).map((item, index) => (
            <div className="card flex flex-col " key={index}>
              <div className="card-header relative ">
                <Link to={`detailmovie/${item.id}`}>
                  {item?.poster_path === null ? (
                    <div className="grid place-content-center h-full bg-slate-100 relative overflow-hidden sm:aspect-[2/3] aspect-square rounded-tl-xl rounded-tr-xl">
                      <img
                        src="https://i.ibb.co/jzxCfDn/brnad.png"
                        alt=""
                        className="w-full object-cover object-top aspect-auto rounded-tl-xl rounded-tr-xl"
                      />
                      <div className="absolute grid place-content-center h-full w-full top-0 left-0 "></div>
                    </div>
                  ) : (
                    <img
                      src={`${IMAGE_BASE_URL}${POSTER_SIZE}` + item.poster_path}
                      alt=""
                      className="w-full object-cover object-top sm:aspect-[2/3] aspect-square rounded-tl-xl rounded-tr-xl"
                    />
                  )}
                </Link>

                <label className="ratting w-[35px] h-[35px] rounded-full grid place-content-center text-white text-sm font-bold border-[2px] border-slate-600 bg-yellow-400 absolute top-2 right-2">
                  {convertRated(item.vote_average)}
                </label>
              </div>
              <div className="card-body flex-1 bg-slate-800 p-2 rounded-bl-xl rounded-br-xl">
                <h1 className="text-sm">{item.title}</h1>
                <ul className="my-4">
                  <li className="flex justify-between items-center">
                    <h1 className="text-slate-600 text-xs">Date:</h1>
                    <p className="text-xs italic">{item.release_date}</p>
                  </li>
                  <li className="flex justify-between items-center">
                    <h1 className="text-slate-600 text-xs">Popularity:</h1>
                    <p className="text-xs italic">{item.popularity}</p>
                  </li>
                </ul>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default MoviePopular;
