const Footer = () => {
  const currentYear = new Date().getFullYear();
  return (
    <footer className="bg-slate-900 ">
      <div className="container mx-auto px-4">
        <p className="text-white text-xs py-3 text-center">
          Copyright © {currentYear}, Movie. All Rights Reserved
        </p>
      </div>
    </footer>
  );
};

export default Footer;
