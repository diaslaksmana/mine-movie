import { Link, NavLink } from "react-router-dom";
import Heart from "../../assets/icon/Heart";
import Home from "../../assets/icon/Home";
import KidsIcon from "../../assets/icon/KidsIcon";
import MovieIcon from "../../assets/icon/MovieIcon";
import Setting from "../../assets/icon/Setting";
import TvIcon from "../../assets/icon/Tv";

const Navigation = () => {
  return (
    <nav className="navigation h-screen flex flex-col justify-between items-center py-3">
      <div className="nav-top">
        <div className="brand">
          <Link to="/">
            <img
              src="https://i.ibb.co/jzxCfDn/brnad.png"
              alt=""
              className="max-w-[60px] max-h-[30px]"
            />
          </Link>
        </div>
      </div>
      <div className="nav-center">
        <ul className="flex flex-col items-center gap-5">
          <li>
            <NavLink to="/">
              <Home /> <span className="title-nav">Home</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/movie">
              <MovieIcon /> <span className="title-nav">Movie</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/tv">
              <TvIcon /> <span className="title-nav">Series</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/kids">
              <KidsIcon /> <span className="title-nav">Kids</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/favorite">
              <Heart /> <span className="title-nav">Favorite</span>
            </NavLink>
          </li>
        </ul>
      </div>
      <div className="nav-bottom">
        <ul className="flex flex-col items-center gap-5">
          <li>
            <NavLink to="/signin">
              <img
                src="https://i.ibb.co/bFNqFCQ/9440461.jpg"
                alt=""
                className="w-[30px] h-[30px] rounded-full m-auto"
              />
            </NavLink>
          </li>
          <li>
            <NavLink to="/setting">
              <Setting />
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navigation;
