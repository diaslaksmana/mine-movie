import { useEffect, useRef, useState } from "react";
import Search from "../../assets/icon/Search";

const Header = () => {
  const [search, setSearch] = useState(false);
  const searchRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        searchRef.current &&
        !searchRef.current.contains(event.target as Node)
      ) {
        setSearch(false);
      }
    };

    document.body.addEventListener("click", handleClickOutside);

    return () => {
      document.body.removeEventListener("click", handleClickOutside);
    };
  }, []);
  return (
    <header id="header" className="fixed right-5 top-5 z-[99999]">
      <div
        ref={searchRef}
        className={`box-search flex items-center bg-white rounded-full overflow-hidden shadow-2xl ${
          search ? "active-search" : ""
        }`}
      >
        <input
          type="text"
          className={`p-3 rounded-full w-0 mr-[-25px] transition-[0.5s] text-black `}
          placeholder="Type here......."
        />
        <button onClick={() => setSearch(!search)} className="px-4 py-2">
          <Search />
        </button>
      </div>
    </header>
  );
};

export default Header;
