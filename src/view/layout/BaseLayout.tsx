import Router from "../../router/Index";
import Footer from "./Footer";
import Header from "./Header";
import Navigation from "./Navigation";

const BaseLayout = () => {
  return (
    <div className="layout flex gap-5">
      <aside className="aside w-[70px] z-10 fixed top-0 left-0 bg-slate-950 h-screen">
        <Navigation />
      </aside>
      <main className="main w-full ml-[70px] ">
        <Header />
        <section className="min-h-[100vh] ">
          <Router />
        </section>
        <Footer />
      </main>
    </div>
  );
};

export default BaseLayout;
