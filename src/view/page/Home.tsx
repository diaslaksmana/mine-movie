import Banner from "../layout/Banner";
import MoviePopular from "../layout/list/MoviePopular";
import TvPopular from "../layout/list/TvPopular";

const Home = () => {
  return (
    <>
      <Banner />
      <MoviePopular />
      <TvPopular />
    </>
  );
};

export default Home;
