import useFetchMovieDetail from "../../../hook/useFetchMovieDetail";
import {
  BACKDROP_SIZE,
  IMAGE_BASE_URL,
  POSTER_SIZE,
  PROFILE_SIZE,
} from "../../../utils/config/Config";
import { convertRated } from "../../../utils/helper/Helper";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Pagination, Autoplay } from "swiper/modules";
import { Link } from "react-router-dom";

const DetailMovie = () => {
  const {
    dataMovie,
    isLoading,
    error,
    genresbyId,
    getProgressColor,
    getRatingPercentage,
    cast,
    handleMoreImage,
    num,
    company,
    review,
    similiar,
  } = useFetchMovieDetail();

  if (isLoading) {
    return <h1>Loading</h1>;
  }
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <section className="relative">
      <div className="h-[85vh] absolute w-full top-0 overflow-hidden after:content-[''] after:backdrop-opacity-85 after:absolute after:h-full after:w-full after:top-0 after:left-0 after:backdrop-blur-md after:brightness-50 z-0">
        {dataMovie?.backdrop_path === null ? (
          <div className="grid place-content-center h-full bg-slate-100 relative overflow-hidden">
            <img
              src="https://i.ibb.co/jzxCfDn/brnad.png"
              alt=""
              className="w-full object-cover object-top sm:aspect-auto aspect-square "
            />
            <h1 className="text-center text-slate-700 text-xl font-semibold">
              No Image
            </h1>
            <div className="absolute grid place-content-center h-full w-full top-0 left-0 "></div>
          </div>
        ) : (
          <img
            src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${dataMovie?.backdrop_path}`}
            className="w-full object-top h-full object-cover"
          />
        )}
      </div>

      <div className="detail relative z-10 pt-[50vh]">
        <div className="container mx-auto py-5 px-5 sm:px-0">
          <div className="block sm:flex gap-10">
            <div className="thumbnail relative basis-1/5 mb-5">
              <div>
                {dataMovie?.poster_path === null ? (
                  <div className="grid place-content-center h-full bg-slate-100 sm:aspect-[2/3] aspect-auto rounded-xl relative overflow-hidden">
                    <img
                      src="https://i.ibb.co/jzxCfDn/brnad.png"
                      alt=""
                      className="w-full object-cover object-top aspect-auto  rounded-tl-xl rounded-tr-xl"
                    />
                    <h1 className="text-center text-slate-700 text-xl font-semibold">
                      <h1 className="text-xl font-semibold">
                        {dataMovie.title}
                      </h1>
                    </h1>
                    <div className="absolute grid place-content-center h-full w-full top-0 left-0 backdrop-opacity-60  backdrop-blur-md"></div>
                  </div>
                ) : (
                  <>
                    <img
                      src={`${IMAGE_BASE_URL}${POSTER_SIZE}${dataMovie?.poster_path}`}
                      className="w-full object-top h-full object-cover shadow-2xl rounded-xl"
                    />
                  </>
                )}

                <div className="detailPage__overviewRating absolute left-2 top-2 ">
                  <svg width="80" height="80">
                    <circle
                      cx="40"
                      cy="40"
                      r="35"
                      stroke="#f1f1f1"
                      strokeWidth="7"
                      fill="none"
                    />
                    <circle
                      cx="40"
                      cy="40"
                      r="35"
                      stroke={getProgressColor(dataMovie?.vote_average)}
                      strokeWidth="7"
                      strokeDasharray={`${getRatingPercentage(
                        dataMovie?.vote_average
                      )} 100`}
                      strokeLinecap="round"
                      fill="none"
                    />
                    <text
                      x="50%"
                      y="60%"
                      textAnchor="middle"
                      fill="#f1f1f1"
                      className="text-2xl font-bold  font-Pridi"
                    >
                      {convertRated(dataMovie?.vote_average)}
                    </text>
                  </svg>
                </div>
                <div className="my-4">
                  <button className="bg-secondary text-primary py-1 px-3 rounded-3xl w-full">
                    Add Favorite
                  </button>
                </div>
              </div>
            </div>
            <div className="overview pb-[100px] basis-4/5">
              <h1 className="title-page font-bold text-5xl mb-5">
                {dataMovie?.title}
              </h1>
              <p className="font-light max-w-[600px] mb-5">
                {dataMovie?.overview}
              </p>
              <h1 className="mb-5">Release: {dataMovie?.release_date}</h1>
              <div className="genre-list flex-wrap text-white mb-5 flex gap-3  ">
                {genresbyId?.map((genre) => (
                  <span key={genre.id} className="text-xs sm:text-md">
                    {genre.name}
                  </span>
                ))}
              </div>
              <div className="cast">
                <h1 className="text-lg">Cast:</h1>
                <div className="flex flex-wrap gap-4 mb-5">
                  {cast?.slice(0, num)?.map((item) => (
                    <div key={item.id} className="card mb-10">
                      <div className="pict-profile text-center h-[80px] w-[80px] ">
                        {item.profile_path === null ? (
                          <div className="grid place-content-center h-[80px] w-[80px] mx-auto bg-slate-100 rounded-full relative overflow-hidden">
                            <img
                              src="https://i.ibb.co/jzxCfDn/brnad.png"
                              alt=""
                              className="object-cover object-top aspect-auto mx-auto"
                            />
                            <div className="absolute grid place-content-center h-full w-full top-0 left-0 backdrop-opacity-60  backdrop-blur-md"></div>
                          </div>
                        ) : (
                          <img
                            src={`${IMAGE_BASE_URL}${PROFILE_SIZE}${item?.profile_path}`}
                            className="h-[80px] w-[80px] mx-auto object-top object-cover shadow-2xl rounded-full"
                          />
                        )}
                        <h1 className="text-xs font-light">{item.name}</h1>
                      </div>
                    </div>
                  ))}
                  <div className="h-[80px] w-[80px] grid place-content-center">
                    {num < cast?.length && (
                      <button
                        className="load-more bg-primary rounded-xl flex items-center p-2 gap-2"
                        onClick={handleMoreImage}
                      >
                        <img
                          src="https://i.ibb.co/kcFqcCj/2199111-add-create-new-plus-icon.png"
                          alt=""
                          className="mx-auto h-[20px] w-[20px]"
                        />
                        More
                      </button>
                    )}
                  </div>
                </div>
                <h1 className="text-lg">Production:</h1>
                <div className="production flex flex-wrap gap-5">
                  {company?.map((item) => (
                    <div key={item.id} className="flex items-center">
                      {item.logo_path === null ? (
                        <div className="grid place-content-center z-10 h-[30px] w-[30px] mx-auto bg-slate-100 rounded-full relative overflow-hidden">
                          <img
                            src="https://i.ibb.co/jzxCfDn/brnad.png"
                            alt=""
                            className="object-cover object-top aspect-auto mx-auto"
                          />
                          <div className="absolute grid place-content-center h-full w-full top-0 left-0 backdrop-opacity-60  backdrop-blur-md"></div>
                        </div>
                      ) : (
                        <img
                          src={`${IMAGE_BASE_URL}${POSTER_SIZE}${item?.logo_path}`}
                          className="w-[30px] h-[30px]  bg-slate-100 rounded-full z-10 object-center object-cover border-[1px] border-slate-100 shadow-2xl "
                        />
                      )}
                      <h1 className="z-0 ml-[-10px] text-xs font-light bg-slate-100 text-primary px-3 rounded-md">
                        {item.name}
                      </h1>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>

          {/* Review */}
          <div className="mb-[50px]">
            <h1 className="text-2xl mb-4">Review</h1>
            {review?.length === 0 ? (
              <h1>No Review</h1>
            ) : (
              <Swiper
                slidesPerView={1}
                spaceBetween={10}
                pagination={{
                  clickable: true,
                }}
                autoplay={{
                  delay: 2500,
                  disableOnInteraction: false,
                }}
                breakpoints={{
                  768: {
                    slidesPerView: 1,
                    spaceBetween: 10,
                  },
                  1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                  },
                }}
                modules={[Pagination, Autoplay]}
                className="mySwiper"
              >
                {review?.map((item) => (
                  <SwiperSlide key={item.id}>
                    <Link to={item.url} target="blank">
                      <div className="card-slider">
                        <div className="bg-white min-h-[150px] rounded-xl">
                          <p className="quotes text-slate-400 text-justify text-sm justify-center ">
                            {item.content.length > 100
                              ? `${item.content.substring(0, 100)}...`
                              : item.content}
                          </p>
                        </div>
                        <div className="text-center mt-[-50px]">
                          <div className="grid place-content-center h-[80px] w-[80px] mx-auto bg-slate-100 rounded-full relative overflow-hidden">
                            <img
                              src="https://i.ibb.co/jzxCfDn/brnad.png"
                              alt=""
                              className="object-cover object-top aspect-auto mx-auto"
                            />
                            <div className="absolute grid place-content-center h-full w-full top-0 left-0 backdrop-opacity-60  backdrop-blur-md"></div>
                          </div>
                          <h1>{item.author}</h1>
                          <p className="flex gap-2 justify-center my-2">
                            {/* @ts-ignore*/}
                            {[...Array(item?.author_details?.rating)].map(
                              (_, index) => (
                                <img
                                  key={index}
                                  src="https://i.ibb.co/GTBQktX/2792947-star-xmas-icon.png"
                                  alt={`Star ${index + 1}`}
                                  className="inline-block h-4 w-4"
                                />
                              )
                            )}
                          </p>
                        </div>
                      </div>
                    </Link>
                  </SwiperSlide>
                ))}
              </Swiper>
            )}
          </div>

          {/* Similiar */}
          <h1 className="text-2xl mb-4">Similiar Movie</h1>
          <div className="grid grid-cols-2 sm:grid-cols-7 gap-5">
            {/* @ts-ignore*/}
            {similiar?.slice(0, 7).map((item, index) => (
              <div className="card flex flex-col " key={index}>
                <div className="card-header relative ">
                  <Link to={`detailmovie/${item.id}`}>
                    {item?.poster_path === null ? (
                      <div className="grid place-content-center h-full bg-slate-100 relative overflow-hidden sm:aspect-[2/3] aspect-square rounded-tl-xl rounded-tr-xl">
                        <img
                          src="https://i.ibb.co/jzxCfDn/brnad.png"
                          alt=""
                          className="w-full object-cover object-top aspect-auto rounded-tl-xl rounded-tr-xl"
                        />
                        <div className="absolute grid place-content-center h-full w-full top-0 left-0 "></div>
                      </div>
                    ) : (
                      <img
                        src={
                          `${IMAGE_BASE_URL}${POSTER_SIZE}` + item.poster_path
                        }
                        alt=""
                        className="w-full object-cover object-top sm:aspect-[2/3] aspect-square rounded-tl-xl rounded-tr-xl"
                      />
                    )}
                  </Link>

                  <label className="ratting w-[35px] h-[35px] rounded-full grid place-content-center text-white text-sm font-bold border-[2px] border-slate-600 bg-yellow-400 absolute top-2 right-2">
                    {convertRated(item.vote_average)}
                  </label>
                </div>
                <div className="card-body flex-1 bg-slate-800 p-2 rounded-bl-xl rounded-br-xl">
                  <h1 className="text-sm">{item.title}</h1>
                  <ul className="my-4">
                    <li className="flex justify-between items-center">
                      <h1 className="text-slate-600 text-xs">Date:</h1>
                      <p className="text-xs italic">{item.release_date}</p>
                    </li>
                    <li className="flex justify-between items-center">
                      <h1 className="text-slate-600 text-xs">Popularity:</h1>
                      <p className="text-xs italic">{item.popularity}</p>
                    </li>
                  </ul>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default DetailMovie;
