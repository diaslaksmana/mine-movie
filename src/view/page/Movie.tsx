import { Link } from "react-router-dom";
import useFetchCategory from "../../hook/useFetchCategory";
import { IMAGE_BASE_URL, POSTER_SIZE } from "../../utils/config/Config";
import { convertRated } from "../../utils/helper/Helper";

const Movie = () => {
  const {
    catMovie,
    listGenreMovie,
    error,
    isLoading,
    handleGenreChange,
    handleSortChange,
  } = useFetchCategory();

  if (isLoading) {
    return <h1>Loading</h1>;
  }
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <section id="movie">
      <div className="container mx-auto py-5 px-5 sm:px-0">
        <h1 className="title-page font-bold text-2xl">Movie Category</h1>
        <div className="box-filter mt-[100px]  lg:flex block gap-10">
          <div className="bg-category">
            <h1>By Category:</h1>

            <select
              onChange={handleGenreChange}
              className="border-[2px] min-w-[150px] border-primary text-primary bg-[transparent] py-2 px-4  hover:outline-0 focus:outline-0"
            >
              <option value="All Category">All Category</option>
              {listGenreMovie.map((item, index) => (
                <option value={item.id} key={index}>
                  {item.name}
                </option>
              ))}
            </select>
          </div>
          <div className="bg-date">
            <h1>By Date:</h1>
            <select className="border-[2px] min-w-[150px] border-primary text-primary bg-[transparent] py-2 px-4  hover:outline-0 focus:outline-0">
              <option value="">Weeks</option>
              <option value="">Days</option>
            </select>
          </div>
          <div className="bg-ratting">
            <h1>By Ratting:</h1>
            <select
              onChange={handleSortChange}
              className="border-[2px] min-w-[150px] border-primary text-primary bg-[transparent] py-2 px-4  hover:outline-0 focus:outline-0"
            >
              <option value="popularity.desc">Popularity Desc</option>
              <option value="rating_lowest">Rating Lowest</option>
              <option value="rating_highest">Rating Highest</option>
            </select>
          </div>
        </div>

        <div className="grid grid-cols-2 sm:grid-cols-5 gap-8 my-10">
          {catMovie?.map((item, index) => (
            <div className="card relative overflow-hidden" key={index}>
              <Link to={`/detailmovie/${item.id}`}>
                {item.poster_path === null ? (
                  <div className="grid place-content-center h-full bg-slate-100 sm:aspect-[2/3] aspect-auto rounded-xl relative overflow-hidden">
                    <img
                      src="https://i.ibb.co/jzxCfDn/brnad.png"
                      alt=""
                      className="w-full object-cover object-top aspect-auto  rounded-tl-xl rounded-tr-xl"
                    />
                    <h1 className="text-center text-slate-700 text-xl font-semibold">
                      <h1 className="text-xl font-semibold">{item.title}</h1>
                    </h1>
                    <div className="absolute grid place-content-center h-full w-full top-0 left-0 backdrop-opacity-60  backdrop-blur-md"></div>
                  </div>
                ) : (
                  <>
                    <img
                      src={`${IMAGE_BASE_URL}${POSTER_SIZE}` + item.poster_path}
                      alt=""
                      className="w-full object-cover object-top sm:aspect-[2/3] aspect-auto rounded-xl "
                    />
                  </>
                )}

                <label className="ratting w-[35px] h-[35px] rounded-full grid place-content-center text-white text-sm font-bold border-[2px] border-slate-600 bg-yellow-400 absolute top-2 right-2">
                  {convertRated(item.vote_average)}
                </label>
              </Link>
            </div>
          ))}
        </div>
        <div className="pagination my-5">
          <ul className="flex items-center justify-center gap-4">
            <li>
              <button className="bg-primary text-white py-2 px-4" disabled>
                next
              </button>
            </li>
            <li>
              <button className="border-[1px] border-primary text-white py-2 px-4 hover:bg-primary active">
                1
              </button>
            </li>
            <li>
              <button className="border-[1px] border-primary text-white hover:bg-primary py-2 px-4 ">
                2
              </button>
            </li>
            <li>...</li>
            <li>
              <button className="bg-primary text-white py-2 px-4">prev</button>
            </li>
          </ul>
        </div>
      </div>
    </section>
  );
};

export default Movie;
