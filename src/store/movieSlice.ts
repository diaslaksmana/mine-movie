import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { MovieModel } from "../utils/interface/movie";

interface MoviesState {
  movies: MovieModel[];
  loading: boolean;
  error: string | null;
}
const initialState: MoviesState = {
  movies: [],
  loading: false,
  error: null,
};
const movieSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {
    fetchMoviesStart: (state) => {
      state.loading = true;
      state.error = null;
    },
    fetchMoviesSuccess: (state, action: PayloadAction<any[]>) => {
      state.loading = false;
      state.movies = action.payload;
    },
    fetchMoviesFailure: (state, action: PayloadAction<string>) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});
export const { fetchMoviesStart, fetchMoviesSuccess, fetchMoviesFailure } =
  movieSlice.actions;

export default movieSlice.reducer;
