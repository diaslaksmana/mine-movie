import { BASE_URL, options } from "../config/Config";

export async function fetchData(endpoint: string) {
  const apiKey = process.env.REACT_APP_API_KEY;
  const response = await fetch(
    `${BASE_URL}/${endpoint}?api_key=${apiKey}&language=en-US,${options}`
  );
  const data = response.json();
  if (!response.ok) {
    throw new Error("Failed to fetch data");
  }
  return data;
}

export async function fetchListGenre(endpoint: string) {
  const apiKey = process.env.REACT_APP_API_KEY;
  const response = await fetch(
    `${BASE_URL}/${endpoint}?api_key=${apiKey}&language=en-US,${options}`
  );
  const data = response.json();
  if (!response.ok) {
    throw new Error("Failed to fetch data");
  }
  return data;
}
