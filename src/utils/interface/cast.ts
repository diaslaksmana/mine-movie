export interface CastModel {
  id: number;
  name: string;
  gender: number;
  character: string;
  profile_path: string;
}
