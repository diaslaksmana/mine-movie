export interface MovieModel {
  type: any;
  id: number;
  title: string;
  name: string;
  backdrop_path: string;
  genre_ids: number[];
  overview: string;
  first_air_date: string;
  popularity: number;
  poster_path: string;
  vote_average: number;
  release_date: number;
  homepage: string;
  vote_count: string;
  media_type: string;
  logo_path: string;
  production_companies: string;
}
export interface GenreModel {
  id: number;
  name: string;
}

export interface GetMoviesResponse {
  genres: MovieModel[];
  id: number;
  name: string;
  children?: JSX.Element | JSX.Element[];
}
