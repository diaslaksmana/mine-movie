export interface ReviewModel {
  id: number;
  author: string;
  name: string;
  avatar_path: string;
  content: string;
  rating: number;
  url: string;
  total_results: number;
  author_details: string;
}
