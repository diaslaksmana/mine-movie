/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#88AB8E",
        secondary: "#EEE7DA",
      },
    },
  },
  plugins: [],
};
